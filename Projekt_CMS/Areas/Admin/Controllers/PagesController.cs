﻿using Projekt_CMS.Models.Data;
using Projekt_CMS.Models.ViewModels.Pages;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Projekt_CMS.Areas.Admin.Controllers
{
    public class PagesController : Controller
    {
        // GET: Admin/Pages
        public ActionResult Index()
        {
            // deklaracja listy PageVM
            List<PageVM> pagesList;

            using (Db db = new Db())
            {
                // inicjalizacja listy, using - oczyszczenie pamięci
                pagesList = db.Pages.ToArray().OrderBy(x => x.Sorting).Select(x => new PageVM(x)).ToList();
            }

            // zwracamy strony do widoku
            return View(pagesList);
        }

        // GET: Admin/Pages/AddPage
        [HttpGet]
        public ActionResult AddPage()
        {
            return View();
        }

        // POST: Admin/Pages/AddPage
        [HttpPost]
        public ActionResult AddPage(PageVM model)
        {
            // sprawdzanie model state
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (Db db = new Db())
            {
                string slug;

                //inicjalizacja PageDTO
                PageDTO dto = new PageDTO();

                // gdy slug pozostanie pusty, przypisujemy tytuł
                if (string.IsNullOrWhiteSpace(model.Slug))
                {
                    slug = model.Title.Replace(" ", "-").ToLower();
                }
                else
                {
                    slug = model.Slug.Replace(" ", "-").ToLower();
                }

                //zapobiegamy dodaniu takiej samej nazwy strony
                if(db.Pages.Any( x => x.Title == model.Title) || db.Pages.Any(x => x.Slug == slug))
                { 
                    ModelState.AddModelError("", "Ten tytuł lub adres strony już istnieje.");
                    return View(model);
                }

                dto.Title = model.Title;
                dto.Slug = slug;
                dto.Body = model.Body;
                dto.HasSidebar = model.HasSidebar;
                dto.Sorting = 1000;

                //zapisanie strony do bazy
                db.Pages.Add(dto);
                db.SaveChanges();
            }

            TempData["SM"] = "Dodałeś nową stronę.";

            return RedirectToAction("Index");
        }

        // GET: Admin/Pages/EditPage/id
        [HttpGet]
        public ActionResult EditPage(int id)
        {
            //deklaracja PageWM
            PageVM model;

            using (Db db = new Db())
            {
                // pobieramy stronę z bazy o przekazanym id
                PageDTO dto = db.Pages.Find(id);

                // sprawdzamy czy taka strona istnieje
                if (dto == null)
                {
                    return Content("Strona nie istnieje");
                }

                model = new PageVM(dto);

            }

           return View(model);
        }

        // POST: Admin/Pages/EditPage
        [HttpPost]
        public ActionResult EditPage(PageVM model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (Db db = new Db())
            {
                //pobranie id ze strony
                int id = model.Id;

                //inicjalizacja slug
                string slug = "home";

                //pobranie strony do edycji
                PageDTO dto = db.Pages.Find(id);
                
                if (model.Slug != "home")
                {
                    if (string.IsNullOrWhiteSpace(model.Slug))
                    {
                        slug = model.Title.Replace(" ", "-").ToLower();
                    }
                    else
                    {
                        slug = model.Slug.Replace(" ", "-").ToLower();
                    }
                }

                //sprawdzenie czy stron jest unikalna
                if(db.Pages.Where(x => x.Id != id).Any(x => x.Title == model.Title)
                    || db.Pages.Where(x => x.Id != id).Any(x => x.Slug == slug))
                {
                    ModelState.AddModelError("", "Strona lub adres strony już istnieje");
                }

                //modyfikacja DTO
                dto.Title = model.Title;
                dto.Slug = slug;
                dto.HasSidebar = model.HasSidebar;
                dto.Body = model.Body;

                //zapis edytowanej strony do bazy
                db.SaveChanges();
            }

            //Short message
            TempData["SM"] = "Edycja strony przebiegła pomyślnie.";

            //redirect
            return RedirectToAction("EditPage");
        }

        // GET: Admin/Pages/Details/id
        [HttpGet]
        public ActionResult Details(int id)
        {
            //deklaracja PageVM
            PageVM model;

            using (Db db = new Db())
            {
                //pobranie strony o danym id
                PageDTO dto = db.Pages.Find(id);

                //sprawdzenie czy strona istnieje
                if (dto == null)
                {
                    return Content("Strona o danym id nie istnieje.");
                }

                //inicjalizacja PageVM
                model = new PageVM(dto);
        
            }

            return View(model);
        }

        // GET: Admin/Pages/Delete/id
        [HttpGet]
        public ActionResult Delete(int id)
        {
            using (Db db = new Db())
            {
                //pobranie strony do usunięcia
                PageDTO dto = db.Pages.Find(id);

                //usuwanie strony
                db.Pages.Remove(dto);

                db.SaveChanges();
            }

            //przekierowawnie
            return RedirectToAction("Index");
        }

        // POST: Admin/Pages/ReorderPages
        [HttpPost]
        public ActionResult ReorderPages(int [] id)
        {
            using ( Db db = new Db())
            {
                int count = 1;
                PageDTO dto;

                //sortowanie
                foreach (var pageId in id)
                {
                    dto = db.Pages.Find(pageId);
                    dto.Sorting = count;

                    db.SaveChanges();
                    count++;
                }
            }
            return View();
        }

        // GET: Admin/Pages/EditSidebar
        [HttpGet]
        public ActionResult EditSidebar()
        {
            //Deklaracja SidebarVM
            SidebarVM model;

            using (Db db = new Db())
            {
                //pobranie sidebar DTO
                SidebarDTO dto = db.Sidebar.Find(1);

                //inicjalizacja modelu
                model = new SidebarVM(dto);
            }

            return View(model);
        }

        // POST: Admin/Pages/EditSidebar
        [HttpPost]
        public ActionResult EditSidebar(SidebarVM model)
        {
            using (Db db = new Db())
            {
                //pobranie sidebar DTO
                SidebarDTO dto = db.Sidebar.Find(1);

                dto.Body = model.Body;
                db.SaveChanges();
            }

            TempData["SM"] = "Edycja strony przebiegła pomyślnie.";

            return RedirectToAction("EditSidebar");
        }
    }
}