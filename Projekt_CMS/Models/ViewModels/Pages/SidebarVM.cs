﻿using Projekt_CMS.Models.Data;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Projekt_CMS.Models.ViewModels.Pages
{
    public class SidebarVM
    {
        public SidebarVM()
        {

        }

        public SidebarVM(SidebarDTO row)
        {
            Id = row.Id;
            Body = row.Body;
        }

        public int Id { get; set; }

        [Display(Name = "Treść:")]
        [AllowHtml]
        public string Body { get; set; }
    }
}